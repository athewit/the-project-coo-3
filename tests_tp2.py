from tp2 import *
#créer des boites
def test_box_create():
    b = Box()

#ajouter un item dans la boite
def test_box_add():
    b = Box()
    b.add('truc')
    b.add('machin')
    assert 'truc' in b
    assert 'machin' in b
    assert 'chose' not in b

#savoir si il y a un item donné dans une boite
def test_in_b():
    b = Box()
    b.add('truc')
    b.add('machin')
    assert 'truc' in b
    assert 'dragon' not in b

#retirer un item d'une boite
def test_retirer():
    b = Box()
    b.add('Excalibur')
    b.add('Murasame')
    assert 'Excalibur' in b
    b.remove('Excalibur')
    assert 'Excalibur' not in b
    assert 'Murasame' in b

#
def test_is_open():
    b = Box()
    assert not b.is_open()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_regarder():
    b = Box()
    b.add("Spaghetti")
    b.add("Gungnir")
    r = b.action_look()
    assert r == "la boite est fermée"
    b.open()
    r = b.action_look()
    assert r == "la boite contient : Spaghetti, Gungnir"

def test_space():
    item = Item(3)
    assert item.volume() == 3

def test_capacite() :
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5

def test_possible_ajout():
    item = Item(3)
    b = Box()
    b.set_capacity(5)
    assert b.has_room(item)

def test_ajout():
    item1 = Item(3)
    item2 = Item(5)
    b = Box()
    b.set_capacity(5)
    b.close()
    assert b.add_item(item1)==False
    b.open()
    assert b.add_item(item1)==True
    assert b.add_item(item2)==False

def test_nom():
    item = Item(3)
    item.set_name("Murasame")
    assert repr(item) == "Objet : Murasame"

def test_has_name():
    item = Item(1000000000)
    item.set_name("It's over nine thousaaaaaaaaaaaaaaaaaands")
    assert item.has_name("It's over nine thousaaaaaaaaaaaaaaaaaands")

def test_contains():
    b = Box()
    item = Item(3,"Murasame")
    assert b.find(item) == None
    b.open()
    b.add_item(item)
    assert b.find("Murasame")

def test_from_data_pers():
    x=Person.from_data({"firstname":"Shiina", "lastname": "Mashiro"})
    assert x._firstname == "Shiina" and x._lastname == "Mashiro"
